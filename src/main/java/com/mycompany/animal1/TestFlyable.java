/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.animal1;

/**
 *
 * @author sairu
 */
public class TestFlyable {

    public static void main(String[] args) {
        Human h1 = new Human("Peem");

        Cat c1 = new Cat("Win");

        Dog d1 = new Dog("Tan");

        Snake s1 = new Snake("Nippon");
        
        Snake s2 = new Snake("Benz");

        Crocodile co1 = new Crocodile("Nat");

        Bird b1 = new Bird("tee");

        Bat bt1 = new Bat("Nan");

        Fish f1 = new Fish("Oil");

        Crab cb1 = new Crab("Fhanglerr");

        Bat bt2 = new Bat("Yu");
        
        Plane plane = new Plane("Engine number 1");
        
        Dog d2 = new Dog("Frame");
        
        Car car = new Car("Engine number 2");
        
        Flyable[] flyable = {b1, bt1, bt2, plane};
        for (Flyable f : flyable) {
            if (f instanceof Plane) {
                Plane p = (Plane) f;
                p.startEngine();
                p.run();
            }
            f.fly();
        }
        System.out.println("");
        Runable[] runable = {h1, c1, d1, d2,car};
        for (Runable r : runable) {
            if (r instanceof Car) {
                Car c = (Car) r;
                c.startEngine();
            }
            r.run();
        }
        System.out.println("");
        Swimable[] swimable = {f1,cb1};
        for (Swimable s : swimable) {
            s.swim();
        }
        System.out.println("");
        Crawlable[] crawlable = {s1,s2,co1};
        for (Crawlable c : crawlable) {
            c.crawl();
        }
    }
}
