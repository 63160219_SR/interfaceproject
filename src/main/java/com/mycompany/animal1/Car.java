/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.animal1;

/**
 *
 * @author sairu
 */
public class Car extends Vahicle implements Runable {

    public Car(String engine) {
        super(engine);
    }

    @Override
    public void startEngine() {
        System.out.println("Car: Start engine");
    }

    @Override
    public void stopEngine() {
        System.out.println("Car: Stop engine");
    }

    @Override
    public void raiseSpeed() {
        System.out.println("Car: Raisespeed");
    }

    @Override
    public void applyBreak() {
        System.out.println("Car: Applybreak");
    }

    @Override
    public void run() {
        System.out.println("Car: run");
    }

}
